import React, { useState, useEffect } from "react";
import axios from "axios";
//import { FaSun, FaCloud, FaCloudRain, FaCloudSun, FaSnowflake } from "react-icons/fa";
import "./WeatherApp.css"; // Importiere deine CSS-Datei für die Stilisierung

const API_KEY = "bbab65efe9daa24f1e07d4b84f362e91";

const WeatherApp = () => {
  const [city, setCity] = useState("");
  const [weatherData, setWeatherData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      setError(null);

      try {
        const response = await axios.get(
          `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`
        );
        setWeatherData(response.data);
      } catch (error) {
        setError(error.message);
      }

      setLoading(false);
    };

    if (city) {
      fetchData();
    }
  }, [city]);

  const getWeatherIcon = (weatherCode) => {
    // Deine Funktion bleibt unverändert
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setWeatherData(null);
    setError(null);
    if (city.trim()) {
      setCity(city.trim());
    } else {
      setError("Bitte geben Sie eine Stadt ein.");
    }
  };

  return (
    <div className="weather-app-container">
      <h1>Wetter App</h1>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Stadt eingeben"
          value={city}
          onChange={(e) => setCity(e.target.value)}
        />
        <button type="submit">Suchen</button>
      </form>
      {loading && <p>Lade...</p>}
      {error && <p>{error}</p>}
      {weatherData && (
        <div>
          <h2>{weatherData.name}</h2>
          <p>Temperatur: {weatherData.main.temp}°C</p>
          <p>Beschreibung: {weatherData.weather[0].description}</p>
          <p>Windgeschwindigkeit: {weatherData.wind.speed} m/s</p>
          <div className="weather-icon">{getWeatherIcon(weatherData.weather[0].id)}</div>
        </div>
      )}
    </div>
  );
};

export default WeatherApp;
